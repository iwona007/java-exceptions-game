package Iwona.pl.logic;

import Iwona.pl.exceptions.AgeViolationException;
import Iwona.pl.exceptions.DuplicateException;
import Iwona.pl.exceptions.MaxCompetitorsException;
import Iwona.pl.model.Participant;

public class Competition {
    private String name;
    private Participant[] participants;
    int ageLimit;
    private int size;

    public Competition(String name, int maxParticipants, int ageLimit) {
        if (ageLimit < 0) {
            throw new IllegalArgumentException("ageLimit musi być liczbą dodatnia");
        }
        this.name = name;
        this.ageLimit = ageLimit;
        this.participants = new Participant[maxParticipants];
    }

    public void addParticipant(Participant participant) {
        if (participant.getAge() > ageLimit) {
            throw new AgeViolationException(ageLimit);
        }
        if ( participants.length == size) {
            throw new MaxCompetitorsException(participants.length);
        }
        findParticipation(participant);
        participants[size] = participant;
        size++;
    }

    private void findParticipation(Participant participant) {
        for (int i = 0; i < size; i++) {
            if (participants[i].getDocumentId().equals(participant.getDocumentId())) {
                throw new DuplicateException(participant);
            }
        }
    }

    public boolean hasFreeSpot() {
        return size < participants.length;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Zawody ")
                .append(name)
                .append("\n")
                .append("Liczba uczestników: ").append(size);
        for (int i = 0; i < size; i++) {
            stringBuilder.append("\n")
                    .append("> ")
                    .append(participants[i].toString());
        }
        return stringBuilder.toString();
    }
}
