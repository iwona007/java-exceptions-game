package Iwona.pl.exceptions;

import Iwona.pl.model.Participant;

public class DuplicateException extends RuntimeException {
//    public DuplicateException(String documentId) {
//        super(String.format("uczestnik o okreslonym dokumencie id %s juz istnieje:", documentId));
//    }

    private Participant participant;

    public DuplicateException(Participant participant) {
        this.participant = participant;
    }

    public Participant getParticipant() {
        return participant;
    }
}
