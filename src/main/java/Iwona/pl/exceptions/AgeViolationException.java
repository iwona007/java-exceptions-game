package Iwona.pl.exceptions;

public class AgeViolationException extends RuntimeException {

//    public AgeViolationException(int ageLimit) {
//        super(String.format("Uczestnik zawodów nie może przekraczać wieku: %s ", ageLimit));
//    }

    private final int ageLimit;

    public AgeViolationException(int ageLimit) {
        this.ageLimit = ageLimit;
    }

    public int getAgeLimit() {
        return ageLimit;
    }
}
