package Iwona.pl.exceptions;

import Iwona.pl.model.Participant;

public class MaxCompetitorsException extends RuntimeException {
//    public MaxCompetitorsException(Participant[] participants) {
//        super(String.format("W zawodach omze wiażśc udział tylo %s liczba zawodników", participants.length));
//    }

    private final int maxParticipates;

    public MaxCompetitorsException(int maxParticipates) {
        this.maxParticipates = maxParticipates;
    }

    public int getMaxParticipates() {
        return maxParticipates;
    }
}
