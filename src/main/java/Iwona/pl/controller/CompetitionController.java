package Iwona.pl.controller;

import Iwona.pl.logic.Competition;
import Iwona.pl.exceptions.AgeViolationException;
import Iwona.pl.exceptions.DuplicateException;
import Iwona.pl.exceptions.MaxCompetitorsException;
import Iwona.pl.model.Participant;
import java.util.InputMismatchException;
import java.util.Scanner;

public class CompetitionController {
    private Scanner scanner = new Scanner(System.in);

    public void run() {
        Competition competition = createCompetition();
        fillParticipantsInfo(competition);
        printCompetition(competition);
    }

    private Competition createCompetition() {
        System.out.println("Podaj nazwę zawodów: ");
        String competitionName = scanner.nextLine();

        System.out.println("Podaj maksymalną liczbę uczestników:");
        int maxParticipants = readPositiveNumber();

        System.out.println("Podaj ograniczenie wiekowe:");
        int ageLimit = readPositiveNumber();

        return new Competition(competitionName, maxParticipants, ageLimit);
    }

    private void fillParticipantsInfo(Competition competition) {
        while (competition.hasFreeSpot()) {
            System.out.println("Dodaj nowego uczestnika");
            Participant participant = createParticipant();

            try {
                competition.addParticipant(participant);
            } catch (AgeViolationException ex) {
                System.out.println("Minimalny wiek uczestnictwa w zawodach to: " + ex.getAgeLimit());
            } catch (DuplicateException ex) {
                System.out.println("Ten uczesnik został już zapisany: " + ex.getParticipant());
            } catch (MaxCompetitorsException ex) {
                System.out.println("Przekroczono maxymalna liczbe uczesników: " + ex.getMaxParticipates());
            }
        }
    }

    private Participant createParticipant() {

        System.out.println("Podaj imię: ");
        String firstName = scanner.nextLine();

        System.out.println("Podaj nazwisko: ");
        String lastName = scanner.nextLine();

        System.out.println("Podaj id (np. pesel): ");
        String documentId = scanner.nextLine();

        System.out.println("Podaj wiek: ");
        int age = readPositiveNumber();

        return new Participant(firstName, lastName, documentId, age);
    }


    private void printCompetition(Competition competition) {
        System.out.println(competition.toString());
    }

    public int readPositiveNumber() {
        int number = -1;

        while (number < 0) {
            try {
                number = scanner.nextInt();
                if (number < 0) {
                    System.out.println("Podana liczba musi być dodatnia");
                }
            } catch (InputMismatchException ex) {
                System.err.println("Nie podałeś liczby. Spróbój jeszcze raz");
            } finally {
                scanner.nextLine();
            }
        }
        return number;
    }
}
